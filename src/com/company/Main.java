package com.company;

import java.util.Base64;

public class Main {

    public static void main(String[] args) {
        String data = "56da65d63d4f15823d1bdb7ca293788d16c8906ee4dfc6975f957bdf38e3e02776562e81a54d9ee766a0891c18d7788db1a89f0c";
        String encodedData = encode(data);
        String decodedData = decode(encodedData);
    }

    private static String encode(String input) {
        return Base64.getEncoder().encodeToString(input.getBytes());
    }

    private static String decode(String input) {
        return new String(Base64.getDecoder().decode(input));
    }
}
